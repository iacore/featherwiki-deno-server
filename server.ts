import Mutex from "./mutex.ts"

// == Configuration

const userpass = null
// use the line bellow to enable basic authentication
// const userpass = "user:pass"

// where the FeatherWiki Wabler file is located
const wikifile = "index.html"

const hostname = "localhost"
const port = 4505

// == Configuration End

const mutex = new Mutex()


function not_authorized() {
  return new Response(null, {
    status: 401,
    headers: {
      "WWW-Authenticate": `Basic realm="FeatherWiki"`,
    },
  })
}

const handler = async (request: Request) => {
  // console.log(request)

  if (userpass != null) {
    const authorization = request.headers.get("Authorization")
    if (!authorization) {
      return not_authorized()
    }
    const matchresult = /Basic (.*)/.exec(authorization)
    if (matchresult) {
      const given_userpass = atob(matchresult[1])
      console.log("Received Authorization:", given_userpass)
      if (given_userpass !== userpass) return not_authorized()
    }
  }

  switch (request.method) {
    case "GET": {
      const id = await mutex.acquire()
      let body
      try {
        body = await Deno.readFile(wikifile)
      } finally {
        mutex.release(id)
      }
      return new Response(body, {
        status: 200,
        headers: { "content-type": "text/html" },
      })
    }
    case "PUT": {
      const id = await mutex.acquire()
      try {
        await Deno.writeFile(wikifile, request.body)
      } finally {
        mutex.release(id)
      }
      return new Response(undefined, { status: 204 })
    }
    case "OPTIONS":
      return new Response(undefined, { status: 200, headers: { dav: "1" } })
    default:
      return new Response(`Invalid HTTP method: ${request.method}`)
  }
}

const server = Deno.serve({ hostname, port }, handler)

await server.finish
