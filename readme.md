## Usage

First clone this repository. Then run the following commands.

```
# use vendored dependencies
deno task run

# auto download dependencies from network
deno run --unstable --allow-net=localhost --allow-read=. --allow-write=. server.ts
```

To enable basic HTTP user+password authentication, uncomment a line in `server.ts`.

`index.html` is a fresh copy of [FeatherWiki Wabler](https://feather.wiki/?page=downloads#warbler). You can replace it with your own copy, maybe another version/fork of FeatherWiki.
